import {ActionReducer, MetaReducer, StoreModule} from "@ngrx/store";
import {localStorageSync} from "ngrx-store-localstorage";
import {NgModule} from "@angular/core";
import {EffectsModule} from "@ngrx/effects";
import {usersReducer} from "./store/users.reducer";
import {UsersEffects} from "./store/users.effects";
import {PicturesEffects} from "./store/pictures.effects";
import {picturesReducer} from "./store/pictures.reducer";

const localStorageSyncReducer = (reducer: ActionReducer<any>) => {
  return localStorageSync({
    keys: [{users: ['user']}],
    rehydrate: true
  })(reducer);
}

const metaReducers: MetaReducer[] = [localStorageSyncReducer];

const reducers = {
  users: usersReducer,
  pictures: picturesReducer,
};

const effects = [UsersEffects, PicturesEffects];

@NgModule({
  imports: [
    StoreModule.forRoot(reducers, {metaReducers}),
    EffectsModule.forRoot(effects),
  ],
  exports: [StoreModule, EffectsModule]
})

export class AppStoreModule {}
