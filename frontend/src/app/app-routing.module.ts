import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {RegisterComponent} from "./pages/register/register.component";
import {LoginComponent} from "./pages/login/login.component";
import {GalleryComponent} from "./pages/gallery/gallery.component";
import {GalleryAuthorComponent} from "./pages/gallery-author/gallery-author.component";
import {NewPictureComponent} from "./pages/new-picture/new-picture.component";

const routes: Routes = [
  {path: '', component: GalleryComponent},
  {path: 'gallery', component: GalleryComponent},
  {path: 'gallery/new', component: NewPictureComponent},
  {path: 'gallery/:id', component: GalleryAuthorComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'login', component: LoginComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
