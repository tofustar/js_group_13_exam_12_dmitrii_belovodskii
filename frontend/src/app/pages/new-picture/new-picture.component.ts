import {Component, ViewChild} from '@angular/core';
import {NgForm} from "@angular/forms";
import {Observable} from "rxjs";
import {PictureData} from "../../models/picture.model";
import {AppState} from "../../store/types";
import {Store} from "@ngrx/store";
import {createPictureRequest} from "../../store/pictures.actions";

@Component({
  selector: 'app-new-picture',
  templateUrl: './new-picture.component.html',
  styleUrls: ['./new-picture.component.sass']
})
export class NewPictureComponent {
  @ViewChild('f') form!: NgForm
  loading: Observable<boolean>
  error: Observable<string | null>

  constructor(private store: Store<AppState>) {
    this.loading = store.select(state => state.pictures.createLoading);
    this.error = store.select(state => state.pictures.createError);
  }

  onSubmit() {
    const pictureData: PictureData = this.form.value;
    this.store.dispatch(createPictureRequest({pictureData}));
  }
}
