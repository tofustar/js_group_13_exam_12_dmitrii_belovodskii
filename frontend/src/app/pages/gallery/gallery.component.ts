import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs";
import {Picture} from "../../models/picture.model";
import {AppState} from "../../store/types";
import {Store} from "@ngrx/store";
import {fetchPicturesRequest} from "../../store/pictures.actions";
import {MatDialog} from "@angular/material/dialog";

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.sass']
})
export class GalleryComponent implements OnInit {
  pictures: Observable<Picture[]>
  loading: Observable<boolean>
  error: Observable<null | string>

  constructor(private store: Store<AppState>, private dialog: MatDialog) {
    this.pictures = store.select(state => state.pictures.pictures);
    this.loading = store.select(state => state.pictures.fetchLoading);
    this.error = store.select(state => state.pictures.fetchError);
  }

  ngOnInit(): void {
    this.store.dispatch(fetchPicturesRequest());
  }

  openModal() {
    this.dialog.open(GalleryComponent);
  }
}

