import { Component, OnInit } from '@angular/core';
import {Picture} from "../../models/picture.model";
import {Observable} from "rxjs";
import {Store} from "@ngrx/store";
import {AppState} from "../../store/types";
import {ActivatedRoute} from "@angular/router";
import {deletePictureRequest, fetchUserPicturesRequest} from "../../store/pictures.actions";
import {User} from "../../models/user.model";

@Component({
  selector: 'app-gallery-author',
  templateUrl: './gallery-author.component.html',
  styleUrls: ['./gallery-author.component.sass']
})
export class GalleryAuthorComponent implements OnInit {
  idAuthor: Observable<string>
  pictures: Observable<Picture[]>
  loading: Observable<boolean>
  error: Observable<null | string>
  user: Observable<null | User>

  authorName = '';
  authorId = '';

  currentUserId: string | undefined = '';

  constructor(private store: Store<AppState>, private route: ActivatedRoute) {
    this.idAuthor = store.select(state => state.pictures.picturesByAuthorId);
    this.pictures = store.select(state => state.pictures.pictures);
    this.loading = store.select(state => state.pictures.fetchLoading);
    this.error = store.select(state => state.pictures.fetchError);
    this.user = store.select(state => state.users.user);
  }

  ngOnInit(): void {
    const idAuthor = this.route.snapshot.params['id'];
    this.store.dispatch(fetchUserPicturesRequest({idAuthor}));
    this.pictures.subscribe(pictureData => {
      pictureData.forEach(picture => {
        this.authorName = picture.user.displayName;
        this.authorId = picture.user._id;
      })
    });
    this.user.subscribe(user => {
      this.currentUserId = user?._id;
      }
    )
  }

  delete(idPicture: string) {
    this.store.dispatch(deletePictureRequest({idPicture}));
  }
}
