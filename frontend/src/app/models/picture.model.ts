import {User} from "./user.model";

export class Picture {
  constructor(
    public id: string,
    public user: User,
    public title: string,
    public image: string,
  ) {
  }
}

export interface ApiPictureData {
  _id: string,
  user: User,
  title: string,
  image: string,
}

export interface PictureData {
  [key: string]: any,
  user: string,
  title: string,
  image: File | null,
}