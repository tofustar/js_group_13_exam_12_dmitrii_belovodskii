import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {LayoutComponent} from "./ui/layout/layout.component";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {FormsModule} from "@angular/forms";
import {MatSidenavModule} from "@angular/material/sidenav";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatListModule} from "@angular/material/list";
import {MatIconModule} from "@angular/material/icon";
import {MatMenuModule} from "@angular/material/menu";
import {MatButtonModule} from "@angular/material/button";
import {FlexLayoutModule} from "@angular/flex-layout";
import {LoginComponent} from "./pages/login/login.component";
import {RegisterComponent} from "./pages/register/register.component";
import {CenteredCardComponent} from "./ui/centered-card/centered-card.component";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatCardModule} from "@angular/material/card";
import {FacebookLoginProvider, SocialAuthServiceConfig, SocialLoginModule} from "angularx-social-login";
import {environment as env} from "../environments/environment";
import {AppStoreModule} from "./app-store.module";
import {MatSnackBarModule} from "@angular/material/snack-bar";
import {GalleryComponent} from './pages/gallery/gallery.component';
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {ImagePipe} from "./pipes/image.pipe";
import {GalleryAuthorComponent} from './pages/gallery-author/gallery-author.component';
import {AuthInterceptor} from "./auth.interceptor";
import {NewPictureComponent} from './pages/new-picture/new-picture.component';
import {FileInputComponent} from "./ui/file-input/file-input.component";
import {MatDialogModule} from "@angular/material/dialog";

const socialConfig: SocialAuthServiceConfig = {
  autoLogin: false,
  providers: [
    {
      id: FacebookLoginProvider.PROVIDER_ID,
      provider: new FacebookLoginProvider(env.fbClientId, {
        scope: 'email,public_profile',
      })
    }
  ]
};

@NgModule({
  declarations: [
    AppComponent,
    ImagePipe,
    FileInputComponent,
    LayoutComponent,
    LoginComponent,
    RegisterComponent,
    CenteredCardComponent,
    GalleryComponent,
    GalleryAuthorComponent,
    NewPictureComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    AppStoreModule,
    BrowserAnimationsModule,
    SocialLoginModule,
    MatSidenavModule,
    MatToolbarModule,
    MatListModule,
    MatIconModule,
    MatMenuModule,
    MatButtonModule,
    MatSnackBarModule,
    FlexLayoutModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatDialogModule,
    MatProgressSpinnerModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    { provide: 'SocialAuthServiceConfig', useValue: socialConfig },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
