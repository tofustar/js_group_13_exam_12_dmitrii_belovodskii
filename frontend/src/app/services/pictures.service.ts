import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {environment as env} from "../../environments/environment";
import {ApiPictureData, Picture, PictureData} from "../models/picture.model";
import {map} from "rxjs";

@Injectable({
  providedIn: 'root'
})

export class PicturesService {
  constructor(private http: HttpClient) {}

  getPictures() {
    return this.http.get<ApiPictureData[]>(env.apiUrl + '/gallery').pipe(
      map(response => {
        return response.map(pictureData => {
          return new Picture(
            pictureData._id,
            pictureData.user,
            pictureData.title,
            pictureData.image,
          );
        });
      })
    );
  }

  getPicturesByAuthor(authorId: string) {
    return this.http.get<ApiPictureData[]>(env.apiUrl + '/gallery?user=' + authorId).pipe(
      map(response => {
        return response.map(pictureData => {
          return new Picture(
            pictureData._id,
            pictureData.user,
            pictureData.title,
            pictureData.image,
          );
        });
      })
    );
  }

  createPicture(pictureData: PictureData) {
    return this.http.post(env.apiUrl + '/gallery', pictureData);
  }

  deletePicture(idPicture: string) {
    return this.http.delete(env.apiUrl + '/gallery/' + idPicture)
  }

}