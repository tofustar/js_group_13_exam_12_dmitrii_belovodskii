import {PicturesState} from "./types";
import {createReducer, on} from "@ngrx/store";
import {
  createPictureFailure,
  createPictureRequest,
  createPictureSuccess,
  deletePictureFailure,
  deletePictureRequest,
  deletePictureSuccess,
  fetchPicturesFailure,
  fetchPicturesRequest,
  fetchPicturesSuccess,
  fetchUserPicturesFailure,
  fetchUserPicturesRequest,
  fetchUserPicturesSuccess
} from "./pictures.actions";

const initialState: PicturesState = {
  pictures: [],
  picturesByAuthorId: '',
  idPicture: '',
  fetchLoading: false,
  fetchError: null,
  createLoading: false,
  createError: null,
};

export const picturesReducer = createReducer(
  initialState,
  on(fetchPicturesRequest, state => ({...state, fetchLoading: true})),
  on(fetchPicturesSuccess, (state, {pictures}) => ({...state, fetchLoading: false, pictures})),
  on(fetchPicturesFailure, (state, {error}) => ({...state, fetchLoading: false, fetchError: error})),

  on(fetchUserPicturesRequest, (state, {idAuthor}) => ({...state, fetchLoading: true, picturesByAuthorId: idAuthor})),
  on(fetchUserPicturesSuccess, (state, {pictures}) => ({...state, fetchLoading: false, pictures})),
  on(fetchUserPicturesFailure, (state, {error}) => ({...state, fetchLoading: false, fetchError: error})),

  on(createPictureRequest, state => ({...state, createLoading: true})),
  on(createPictureSuccess, state => ({...state, createLoading: false})),
  on(createPictureFailure, (state, {error}) => ({...state, createLoading: false, createError: error})),

  on(deletePictureRequest, (state, {idPicture}) => ({...state, idPicture})),
  on(deletePictureSuccess, state => ({...state })),
  on(deletePictureFailure, (state, {error}) => ({...state, fetchError: error})),
)