import {createAction, props} from "@ngrx/store";
import {Picture, PictureData} from "../models/picture.model";

export const fetchPicturesRequest = createAction('[Pictures] Fetch Request');
export const fetchPicturesSuccess = createAction('[Pictures] Fetch Success', props<{pictures: Picture[]}>());
export const fetchPicturesFailure = createAction('[Pictures] Fetch Request', props<{error: string}>());

export const fetchUserPicturesRequest = createAction('[Pictures] Fetch User Request', props<{idAuthor: string}>());
export const fetchUserPicturesSuccess = createAction('[Pictures] Fetch User Success', props<{pictures: Picture[]}>());
export const fetchUserPicturesFailure = createAction('[Pictures] Fetch User Failure', props<{error: string}>());

export const createPictureRequest = createAction('[Pictures] Create Request', props<{pictureData: PictureData}>());
export const createPictureSuccess = createAction('[Pictures] Create Success');
export const createPictureFailure = createAction('[Pictures] Create Failure', props<{error: string}>());

export const deletePictureRequest = createAction('[Pictures] Delete Picture Request', props<{idPicture: string}>());
export const deletePictureSuccess = createAction('[Pictures] Delete Picture Success');
export const deletePictureFailure = createAction('[Pictures] Delete Picture Failure', props<{error: string}>());