import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {PicturesService} from "../services/pictures.service";
import {
  createPictureFailure,
  createPictureRequest, createPictureSuccess, deletePictureFailure, deletePictureRequest, deletePictureSuccess,
  fetchPicturesFailure,
  fetchPicturesRequest,
  fetchPicturesSuccess, fetchUserPicturesFailure,
  fetchUserPicturesRequest, fetchUserPicturesSuccess
} from "./pictures.actions";
import {catchError, map, mergeMap, of, tap} from "rxjs";
import {Router} from "@angular/router";
import {HelpersService} from "../services/helpers.service";

@Injectable()

export class PicturesEffects{
  constructor(
    private actions: Actions,
    private picturesService: PicturesService,
    private router: Router,
    private helpers: HelpersService,
  ) {}

  fetchPictures = createEffect(() => this.actions.pipe(
    ofType(fetchPicturesRequest),
    mergeMap(() => this.picturesService.getPictures().pipe(
      map(pictures => fetchPicturesSuccess({pictures})),
      catchError(() => of(fetchPicturesFailure({
        error: 'Something went wrong'
      })))
    ))
  ));

  fetchPicturesByAuthorId = createEffect(() => this.actions.pipe(
    ofType(fetchUserPicturesRequest),
    mergeMap(({idAuthor}) => this.picturesService.getPicturesByAuthor(idAuthor).pipe(
      map(pictures => fetchUserPicturesSuccess({pictures})),
      catchError(() => of(fetchUserPicturesFailure({error: 'Something went wrong'})))
    ))
  ));

  createPicture = createEffect(() => this.actions.pipe(
    ofType(createPictureRequest),
    mergeMap(({pictureData}) => this.picturesService.createPicture(pictureData).pipe(
      map(() => createPictureSuccess()),
      tap(() => {
        this.helpers.openSnackbar('Your picture has been added');
        void this.router.navigate(['/']);
      }),
      catchError(() => of(createPictureFailure({error: 'Wrong data'})))
    ))
  ));

  deletePicture = createEffect(() => this.actions.pipe(
    ofType(deletePictureRequest),
    mergeMap(({idPicture}) => this.picturesService.deletePicture(idPicture).pipe(
      map(()=> deletePictureSuccess()),
      catchError(() => of(deletePictureFailure({error: 'Something went wrong'})))
    ))
  ))
}