const path = require('path');

const rootPath = __dirname;

module.exports = {
  rootPath,
  uploadPath: path.join(rootPath, 'public/uploads'),
  mongo: {
    db: 'mongodb://localhost/gallery',
    options: {useNewUrlParser: true},
  },
  facebook: {
    appId: '987541708801903',
    appSecret: 'acd9114bedab0aba598452203409ed70'
  }
};