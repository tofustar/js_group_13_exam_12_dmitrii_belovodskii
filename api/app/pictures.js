const express = require('express');
const fs = require('fs').promises;
const mongoose = require('mongoose');
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');
const auth = require('../middleware/auth');
const config = require('../config');
const Picture = require('../models/Picture');

const router = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname))
  }
});

const upload = multer({storage});

router.get('/', async (req, res, next) => {
  try {
    const filter = {};

    if (req.query.user) {
      filter.user = req.query.user;
    }

    const pictures = await Picture.find(filter).populate('user', 'displayName');


    return res.send(pictures);

  } catch (e) {
    return next(e);
  }
});

router.post('/', auth, upload.single('image'), async (req, res, next) => {
  try {

    const pictureData = {
      user: req.user,
      title: req.body.title,
      image: null,
    };

    if(req.file) {
      pictureData.image = req.file.filename;
    } else {
      return res.status(400).send({message: 'Image file is required'});
    }

    const picture = new Picture(pictureData);
    await picture.save();

    return res.send({message: 'Created new picture', id: picture._id});
  } catch (e) {
    if (e instanceof mongoose.Error.ValidationError) {
      if(req.file) {
        await fs.unlink(req.file.path);
      }

      return res.status(400).send(e);
    }

    next(e);
  }
});

router.delete('/:id', async (req, res, next) => {
  try {

    const picture = await Picture.findOne(req.body._id);

    await picture.remove();

    return res.send('Delete complete');
  } catch (e) {
    next(e);
  }
})

module.exports = router;