const mongoose = require('mongoose');
const config = require('./config');
const {nanoid} = require('nanoid');
const Picture = require('./models/Picture');
const User = require("./models/User");

const run = async () => {
  await mongoose.connect(config.mongo.db, config.mongo.options);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [John, Jane, Alex] = await User.create({
      email: 'john@test.com',
      password: '123',
      token: nanoid(),
      displayName: 'John',
    },
    {
      email: 'jain@test.com',
      password: 'qwe',
      token: nanoid(),
      displayName: 'Jain',
    },

    {
      email: 'alex@test.com',
      password: 'zxc',
      token: nanoid(),
      displayName: 'Alex',
    })

  await Picture.create({
      user: John,
      title: 'Sky, tree, sunset.',
      image: 'john_first_pic.jpg',
    },
    {
      user: John,
      title: 'Mountains, glade, flowers.',
      image: 'john_second_pic.jpg',
    },
    {
      user: John,
      title: 'Sea, mountains.',
      image: 'john_third_pic.jpg',
    },
    {
      user: Jane,
      title: 'Beach, palm trees.',
      image: 'jain_first_pic.jpg',
    },
    {
      user: Jane,
      title: 'Beautiful sunset.',
      image: 'jain_second_pic.jpg',
    },
    {
      user: Alex,
      title: 'Birches, river, road.',
      image: 'alex_first_pic.jpg',
    },
    {
      user: Alex,
      title: 'Waterfall.',
      image: 'alex_second_pic.jpg',
    },
  )


  await mongoose.connection.close();
};

run().catch(e => console.error(e));